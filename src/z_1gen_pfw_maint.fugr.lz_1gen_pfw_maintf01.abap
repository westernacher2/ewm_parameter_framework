*----------------------------------------------------------------------*
***INCLUDE LZ_1GEN_PFW_MAINTF01.
*----------------------------------------------------------------------*
FORM z_1gen_parc_create_new ##CALLED.
  TRY.
      zcl_1gen_param_checks=>check_param_tran_create_new( zewm_vpf_parc ).
    CATCH zcx_1gen_common INTO DATA(lx_error).
      MESSAGE lx_error TYPE lx_error->if_t100_dyn_msg~msgty.
  ENDTRY.
ENDFORM.

FORM z_1gen_parmd_create_new ##CALLED.
  TRY.
      zcl_1gen_param_checks=>check_param_no_tran_create_new( zewm_vpf_parmd ).
    CATCH zcx_1gen_common INTO DATA(lx_error).
      MESSAGE lx_error TYPE lx_error->if_t100_dyn_msg~msgty.
  ENDTRY.
ENDFORM.

FORM z_1gen_pardf_create_new ##CALLED.
  TRY.
      zcl_1gen_param_checks=>check_param_define_create_new( zewm_vpf_pardf ).
    CATCH zcx_1gen_common INTO DATA(lx_error).
      MESSAGE lx_error TYPE lx_error->if_t100_dyn_msg~msgty.
  ENDTRY.
ENDFORM.
