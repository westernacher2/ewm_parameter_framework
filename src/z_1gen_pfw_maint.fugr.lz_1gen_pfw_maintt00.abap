*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 07.05.2020 at 18:26:39
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZEWM_VPF_PARC...................................*
TABLES: ZEWM_VPF_PARC, *ZEWM_VPF_PARC. "view work areas
CONTROLS: TCTRL_ZEWM_VPF_PARC
TYPE TABLEVIEW USING SCREEN '0002'.
DATA: BEGIN OF STATUS_ZEWM_VPF_PARC. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZEWM_VPF_PARC.
* Table for entries selected to show on screen
DATA: BEGIN OF ZEWM_VPF_PARC_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZEWM_VPF_PARC.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_VPF_PARC_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZEWM_VPF_PARC_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZEWM_VPF_PARC.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_VPF_PARC_TOTAL.

*...processing: ZEWM_VPF_PARDF..................................*
TABLES: ZEWM_VPF_PARDF, *ZEWM_VPF_PARDF. "view work areas
CONTROLS: TCTRL_ZEWM_VPF_PARDF
TYPE TABLEVIEW USING SCREEN '0001'.
DATA: BEGIN OF STATUS_ZEWM_VPF_PARDF. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZEWM_VPF_PARDF.
* Table for entries selected to show on screen
DATA: BEGIN OF ZEWM_VPF_PARDF_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZEWM_VPF_PARDF.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_VPF_PARDF_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZEWM_VPF_PARDF_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZEWM_VPF_PARDF.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_VPF_PARDF_TOTAL.

*...processing: ZEWM_VPF_PARMD..................................*
TABLES: ZEWM_VPF_PARMD, *ZEWM_VPF_PARMD. "view work areas
CONTROLS: TCTRL_ZEWM_VPF_PARMD
TYPE TABLEVIEW USING SCREEN '0003'.
DATA: BEGIN OF STATUS_ZEWM_VPF_PARMD. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZEWM_VPF_PARMD.
* Table for entries selected to show on screen
DATA: BEGIN OF ZEWM_VPF_PARMD_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZEWM_VPF_PARMD.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_VPF_PARMD_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZEWM_VPF_PARMD_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZEWM_VPF_PARMD.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZEWM_VPF_PARMD_TOTAL.

*.........table declarations:.................................*
TABLES: ZEWM_PFW_PARC                  .
TABLES: ZEWM_PFW_PARDF                 .
TABLES: ZEWM_PFW_PARMD                 .
