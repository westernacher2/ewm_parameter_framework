* regenerated at 07.05.2020 18:26:39
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE lz_1gen_pfw_mainttop.              " Global Declarations
  INCLUDE lz_1gen_pfw_maintuxx.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZ_1GEN_PFW_MAINTF...              " Subroutines
* INCLUDE LZ_1GEN_PFW_MAINTO...              " PBO-Modules
* INCLUDE LZ_1GEN_PFW_MAINTI...              " PAI-Modules
* INCLUDE LZ_1GEN_PFW_MAINTE...              " Events
* INCLUDE LZ_1GEN_PFW_MAINTP...              " Local class implement.
* INCLUDE LZ_1GEN_PFW_MAINTT99.              " ABAP Unit tests
  INCLUDE lz_1gen_pfw_maintf00                    . " subprograms
  INCLUDE lz_1gen_pfw_maintf01.
  INCLUDE lz_1gen_pfw_mainti00                    . " PAI modules
  INCLUDE lsvimfxx                                . " subprograms
  INCLUDE lsvimoxx                                . " PBO modules
  INCLUDE lsvimixx                                . " PAI modules
