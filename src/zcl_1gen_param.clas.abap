CLASS zcl_1gen_param DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES zif_1gen_param_c .

    CLASS-METHODS get_parameter
      IMPORTING
        !iv_lgnum     TYPE /scwm/lgnum OPTIONAL
        !iv_process   TYPE z_1gen_de_pfw_param_id1
        !iv_parameter TYPE z_1gen_de_pfw_param_id2
      EXPORTING
        ev_constant   TYPE z_1gen_de_pfw_param_low
        et_list       TYPE z_1gen_tt_pfw_param_list
        et_range      TYPE rseloption.

  PROTECTED SECTION.

  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_1GEN_PARAM IMPLEMENTATION.


  METHOD get_parameter.
    BREAK-POINT ID zewm_1gen_pfw.

    CLEAR: ev_constant, et_list, et_range.

    "Generic buffering is in play for both tables below
    SELECT *
        FROM zewm_pfw_parmd
        INTO TABLE @DATA(lt_parameter)
        WHERE lgnum = @iv_lgnum
          AND id1 = @iv_process
          AND id2 = @iv_parameter.

    IF sy-subrc <> 0.
      SELECT *
          FROM zewm_pfw_parc
          INTO TABLE @DATA(lt_parameter_c)
          WHERE lgnum = @iv_lgnum
            AND id1 = @iv_process
            AND id2 = @iv_parameter.

      IF sy-subrc <> 0.
        RETURN.
      ENDIF.

      lt_parameter = CORRESPONDING #( BASE ( lt_parameter ) lt_parameter_c ).
    ENDIF.

    "Fill in requested return values
    LOOP AT lt_parameter ASSIGNING FIELD-SYMBOL(<ls_parameter>).
      IF sy-tabix = 1 AND ev_constant IS REQUESTED.
        ev_constant = <ls_parameter>-low.
      ENDIF.

      IF et_list IS REQUESTED.
        APPEND <ls_parameter>-low TO et_list.
      ENDIF.

      IF et_range IS REQUESTED.
        APPEND VALUE #( sign = <ls_parameter>-sign
                        option = <ls_parameter>-opt
                        low = <ls_parameter>-low
                        high = <ls_parameter>-high ) TO et_range.
      ENDIF.
    ENDLOOP.

    "Delete empty ranges
    IF et_range IS REQUESTED.
      DELETE et_range
        WHERE sign IS INITIAL
           OR option IS INITIAL.
    ENDIF.
  ENDMETHOD.
ENDCLASS.
