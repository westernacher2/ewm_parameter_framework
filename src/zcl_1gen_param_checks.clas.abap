CLASS zcl_1gen_param_checks DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES zif_1gen_param_c .

    TYPES:
      BEGIN OF ts_param_tab,
        absolute_name TYPE abap_abstypename,
        value         TYPE char40,
      END OF ts_param_tab .
    TYPES:
      tth_param_tab TYPE HASHED TABLE OF ts_param_tab WITH UNIQUE KEY absolute_name value .

    CLASS-METHODS class_constructor .
    CLASS-METHODS check_param_tran_create_new
      IMPORTING
        !is_parc TYPE zewm_vpf_parc
      RAISING
        zcx_1gen_common .
    CLASS-METHODS check_param_no_tran_create_new
      IMPORTING
        !is_parmd TYPE zewm_vpf_parmd
      RAISING
        zcx_1gen_common .

    CLASS-METHODS check_param_define_create_new
      IMPORTING
        !is_pardf TYPE zewm_vpf_pardf
      RAISING
        zcx_1gen_common .
  PROTECTED SECTION.
  PRIVATE SECTION.

    CLASS-DATA: st_parameter_md      TYPE tth_param_tab,
                sv_id1_absolute_name TYPE abap_abstypename,
                sv_id2_absolute_name TYPE abap_abstypename.
ENDCLASS.



CLASS ZCL_1GEN_PARAM_CHECKS IMPLEMENTATION.


  METHOD check_param_define_create_new.
    BREAK-POINT ID zewm_1gen_pfw.

    IF is_pardf-id1 IS INITIAL
      OR is_pardf-id2 IS INITIAL.
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e002(zewm_1gen_pfw).
    ENDIF.

    "Process ID
    IF NOT line_exists( st_parameter_md[ absolute_name = sv_id1_absolute_name
                                         value = is_pardf-id1 ] ).
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e004(zewm_1gen_pfw) WITH is_pardf-id1.
    ENDIF.

    "Parameter ID
    IF NOT line_exists( st_parameter_md[ absolute_name = sv_id2_absolute_name
                                         value = is_pardf-id2 ] ).
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e005 WITH is_pardf-id2.
    ENDIF.

    "Check description
    IF is_pardf-remark IS INITIAL.
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE w006(zewm_1gen_pfw) WITH is_pardf-id1 is_pardf-id2.
    ENDIF.
  ENDMETHOD.


  METHOD check_param_no_tran_create_new.
    BREAK-POINT ID zewm_1gen_pfw.

    IF is_parmd-id1 IS INITIAL
      OR is_parmd-id2 IS INITIAL
      OR is_parmd-lgnum IS INITIAL
      OR is_parmd-seqno IS INITIAL.
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e002.
    ENDIF.

    SELECT @abap_true
        FROM zewm_pfw_pardf
        INTO  @DATA(lv_existance_check) ##NEEDED
        UP TO 1 ROWS
        WHERE id1 = @is_parmd-id1
          AND id2 = @is_parmd-id2.
    ENDSELECT.

    IF sy-subrc  <> 0.
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e001 WITH is_parmd-id1
        is_parmd-id2.
    ENDIF.
  ENDMETHOD.


  METHOD check_param_tran_create_new.
    BREAK-POINT ID zewm_1gen_pfw.

    IF is_parc-id1 IS INITIAL
      OR is_parc-id2 IS INITIAL
      OR is_parc-lgnum IS INITIAL
      OR is_parc-seqno IS INITIAL.
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e002.
    ENDIF.

    SELECT @abap_true
        FROM zewm_pfw_pardf
        INTO @DATA(lv_existance_check) ##NEEDED
        UP TO 1 ROWS
        WHERE id1 = @is_parc-id1
          AND id2 = @is_parc-id2.
    ENDSELECT.

    IF sy-subrc  <> 0.
      RAISE EXCEPTION TYPE zcx_1gen_common
        MESSAGE e001 WITH is_parc-id1
        is_parc-id1.
    ENDIF.
  ENDMETHOD.


  METHOD class_constructor.
    DATA:
      lo_typedesc_const TYPE REF TO cl_abap_typedescr,
      lo_classdesc      TYPE REF TO cl_abap_classdescr,
      lv_const_name     TYPE string,
      ls_padf           TYPE zewm_pfw_pardf.

    FIELD-SYMBOLS:
    <ls_val>       TYPE data.

    BREAK-POINT ID zewm_1gen_pfw.

    CLEAR: st_parameter_md.

    "Get all attributes of the class (incl. interface)
    lo_classdesc = CAST cl_abap_classdescr( cl_abap_classdescr=>describe_by_object_ref(
                                            NEW zcl_1gen_param_checks( ) ) ).

    LOOP AT lo_classdesc->attributes ASSIGNING FIELD-SYMBOL(<ls_attribute>).
      IF NOT (      <ls_attribute>-is_interface = abap_true
                AND <ls_attribute>-is_constant = abap_true ).
        CONTINUE.
      ENDIF.
      lv_const_name = <ls_attribute>-name.
      REPLACE '~' IN lv_const_name WITH '=>'.

      ASSIGN (lv_const_name) TO <ls_val>.
      IF sy-subrc <> 0.
        CONTINUE.
      ENDIF.

      lo_typedesc_const = cl_abap_datadescr=>describe_by_data( <ls_val> ).
      COLLECT VALUE ts_param_tab( absolute_name = lo_typedesc_const->absolute_name
                                  value = CONV #( <ls_val> ) )
                                  INTO st_parameter_md.
    ENDLOOP.

    "Determine types of ID fields
    lo_typedesc_const = cl_abap_datadescr=>describe_by_data( ls_padf-id1 ).
    sv_id1_absolute_name = lo_typedesc_const->absolute_name.

    lo_typedesc_const = cl_abap_datadescr=>describe_by_data( ls_padf-id2 ).
    sv_id2_absolute_name = lo_typedesc_const->absolute_name.
  ENDMETHOD.
ENDCLASS.
